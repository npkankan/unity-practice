﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeScript : MonoBehaviour {
	// integer, float, boolean, vector
	public bool happy;
	public int myNumber = 10;
	public float myFraction;
	public string myText;
	// Use this for initialization
	void Start () {
		if (happy == true) {
			print (myText);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
